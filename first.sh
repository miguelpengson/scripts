#!/usr/local/bin/bash
# creates a directory and initializes git, .gitignore and venv - python3
# usage: first.sh folder_you_want_to_create

mkdir $1 && cd $1
pwd
git init
python3 -m venv venv
touch .gitignore
echo __pycache__/ > .gitignore
echo .vscode >> .gitignore
source venv/bin/activate
exec /usr/local/bin/bash
